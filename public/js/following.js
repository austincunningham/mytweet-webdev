/**
 * Created by austin on 23/12/2016.
 */
var dateSort = [];
$(window).load(function () {
  $(function () {
    $.get('/followingid', function (data) {
      $.each(data, function (index, element) {
        console.log(data, index, element);
        $.ajax({
          dataType: 'json',
          url: '/following',
          type: 'POST',
          data: element,

          success: function (data) {
            console.log('all data ' + data.name);
            //populateTableRow(data);
            callback(data);
          },

          error: function (err) {
            console.log('fail');
            console.log(err.statusText);
          },
        });

      });
    });
  });
});

function callback(data) {
  //dateSort.concat(data);
  dateSort = dateSort.concat(data);
  bydate(dateSort);// store the array of data in a global for later use
  //populateTable();  // within view
}

function bydate(dateSort) {
  //$.each(dateSort, function (i, val) {
  //console.log('what is val ' + val);
  dateSort = dateSort.sort(Comparator);

  function Comparator(a, b) {
    if (a.date < b.date) return -1;
    if (a.date > b.date) return 1;
    return 0;
  };

  populateTableRow(dateSort);
}

function clearTable() {
  document.getElementById('tablebody').innerHTML = '';
}

function populateTableRow(data)
{
  clearTable();
  for (var i = 0; i < data.length; i++) {
    $('.tablebody ').append('<tr><td><b>' + data[i].name + '</b>  </td><td>'
        + data[i].message + '</td><td >' + new Date(data[i].date).toGMTString() + '</td><td><form action=\"/unfollow/' + data[i].tweeter._id + '\" class=\"ui form\" method=\"delete\"><button  class=\"ui blue active button\"><i class=\"icon add user\"><\/i> Un-Follow<\/button>' + data[i].name + '<\/form> <\/td><\/tr>');

    console.log(data[i]._id);
  }
};


