# README MyTweet #


### What is this repository for? ###

* Quick summary this is a web app built with Node JS which mimics the functionality of Twitter 
* User can signup, Login. 
* User can Add Tweets, delete one or many of there own tweets.
* User can search for other users tweets and look at all tweets
* User can follow/unfollow other users tweets
* User can view a sorted timeline of followed tweets
* User: homer@simpson.com
  Password : secret
* Admin can add and delete users, remove users tweets and look at stats
  Admin: admin@mytweet.com
  Password : secret
* [API documentation](API.md) 
* Version  mytweet-webdev-v04
* [Local version] (http://localhost:4000/)
* [Live version AWS-EC2] (http://35.160.157.123:4000/)

### How do I get set up? ###

* Summary of set up, git clone https https://austincunningham@bitbucket.org/austincunningham/mytweet-webdev.git or ssh git@bitbucket.org:austincunningham/mytweet-webdev.git
  'cd mytweet-web/' and run 'npm install' and 'node index' to start the server  
* Dependencies Node js, npm and MongoDB
* Database configuration current config is pointing to locally hosted mongo DB

### Who do I talk to? ###

* Repo owner or admin austincunningham@oceanfree.net