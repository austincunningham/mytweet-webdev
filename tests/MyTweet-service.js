/**
 * Created by austin on 13/11/2016.
 */
'use strict';

const SyncHttpService = require('./sync-http-service');
const baseUrl = 'http://localhost:4000';

class MyTweetService {

  constructor(baseUrl) {
    this.httpService = new SyncHttpService(baseUrl);
  }

  getTweets() {
    return this.httpService.get('/api/tweets');
  }

  getTweetid(id) {
    return this.httpService.get('/api/tweet/' + id);
  }

  createTweets(id, obj) {
    return this.httpService.post('/api/tweet/' + id, obj);
  }

  getUserTweetsById(id) {
    return this.httpService.get('/api/tweets/' + id);
  }

  getUserTweetsByEmail(email) {
    return this.httpService.get('/api/tweets/email/' + email);
  }

  deleteTweetById(id) {
    return this.httpService.delete('/api/tweets/' + id);
  }

  deleteTweetByEmail(email) {
    return this.httpService.delete('/api/tweets/email/' + email);
  }

  deleteAllTweets() {
    return this.httpService.delete('/api/tweets');
  }

  getTweetCount() {
    return this.httpService.get('/api/tweetcount/');
  }

  createTweetByUserId(id, obj) {
    return this.httpService.post('/api/tweet/' + id, obj);
  }

  deleteTweetByUuid(uuid) {
    return this.httpService.delete('/api/tweets/uuid/' + uuid);
  }

  getUsers() {
    return this.httpService.get('/api/users');
  }

  getUser(id) {
    return this.httpService.get('/api/users/' + id);
  }

  getOneUserByEmail(email) {
    return this.httpService.get('/api/users/email/' + email);
  }

  createUser(newUser) {
    return this.httpService.post('/api/users/register', newUser);
  }

  deleteOneUser(id) {
    return this.httpService.delete('/api/users/' + id);
  }

  deleteOneUserByEmail(email) {
    return this.httpService.delete('/api/users/email/' + email);
  }

  deleteAllUsers() {
    return this.httpService.delete('/api/users');
  }

  userLoginboolean(obj) {
    return this.httpService.post('/api/users/login', obj);
  }

  userLoginAndroid(obj) {
    return this.httpService.post('/api/users/androidLogin', obj);
  }

  userFollow(id, obj) {
    return this.httpService.post('/api/users/follow/' + id, obj);
  }

  userUnfollow(id, obj) {
    return this.httpService.post('/api/users/unfollow/' + id, obj);
  }

  settings(obj) {
    return this.httpService.post('/api/users/settings', obj);
  }

  login(user) {
    return this.httpService.setAuth('/api/users/login', user);
  }

  logout() {
    this.httpService.clearAuth();
  }

}

module.exports = MyTweetService;
