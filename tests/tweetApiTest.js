/**
 * Created by austin on 13/12/2016.
 */
'use strict';

const assert = require('chai').assert;
const MyTweetService = require('./MyTweet-service');
const fixtures = require('./fixtures.json');
const _ = require('lodash');

suite('Tweets API test', function () {

  let users = fixtures.users;
  let tweets = fixtures.tweets;
  let newTweet = fixtures.newTweet;
  let newUser = fixtures.newUser;

  const mytweetService = new MyTweetService('http://localhost:4000');

  beforeEach(function () {
    mytweetService.createUser(newUser);
    mytweetService.login(newUser);
    mytweetService.deleteAllTweets();
    mytweetService.deleteAllUsers();
  });

  afterEach(function () {
    mytweetService.deleteAllUsers();
    mytweetService.deleteAllTweets();
    mytweetService.logout();
  });

  test('get all tweets', function () {


    for (let c of users) {
      const c1 =mytweetService.createUser(c);
      mytweetService.login(c);
      mytweetService.createTweets(c1._id, newTweet);
    }

    const allTweets = mytweetService.getTweets();
    assert.equal(allTweets.length, tweets.length);
  });

  test('get tweet', function () {
    const c1 = mytweetService.createUser(newUser);
    mytweetService.login(newUser);
    const t1 = mytweetService.createTweets(c1._id);
    const t2 = mytweetService.getTweets();
    assert.deepEqual(t1, t2[0]);
  });

  test('get tweet by id', function () {
    const c1 = mytweetService.createUser(newUser);
    mytweetService.login(newUser);
    const t1 = mytweetService.createTweets(c1._id);
    const t2 = mytweetService.getTweetid(t1._id);
    assert.deepEqual(t1, t2);
  });

  test('get tweet invalid id', function () {
    mytweetService.createUser(newUser);
    mytweetService.login(newUser);
    const c1 = mytweetService.getTweetid('1234');
    assert.isNull(c1);
    const c2 = mytweetService.getTweetid('012345678901234567890123');//making sure mongoose api doesn't return positive for correct key length
    assert.isNull(c2);
    const c3 = mytweetService.getTweetid(null);
    assert.isNull(c3);
  });

  test('get tweet by user id', function () {
    const c1 = mytweetService.createUser(newUser);
    mytweetService.login(newUser);
    newTweet.tweeter = c1._id;
    const t1 = mytweetService.createTweets(c1._id, newTweet);
    const t2 = mytweetService.getUserTweetsById(c1._id);//looking for tweeter:id
    assert.deepEqual(t1, t2[0]);
  });

  test('get tweet invalid user id', function () {
    mytweetService.createUser(newUser);
    mytweetService.login(newUser);
    const c1 = mytweetService.getUserTweetsById('1234');
    assert.isNull(c1);
    const c2 = mytweetService.getUserTweetsById('012345678901234567890123');//making sure mongoose api doesn't return positive for correct key length
    assert.equal(c2.length, 0);
  });

  test('get tweet by user email', function () {
    const c1 = mytweetService.createUser(newUser);
    mytweetService.login(newUser);
    newTweet.tweeter = c1._id;
    const t1 = mytweetService.createTweets(c1._id, newTweet);
    const t2 = mytweetService.getUserTweetsByEmail(t1.name);//looking for tweeter:id
    assert.deepEqual(t1, t2[0]);
  });

  test('get tweet invalid email', function () {
    const c1 = mytweetService.createUser(newUser);
    mytweetService.login(newUser);
    newTweet.tweeter = c1._id;
    const t1 = mytweetService.createTweets(c1._id, newTweet);
    const c = mytweetService.getUserTweetsByEmail('a@a.com');
    assert.equal(c.length, 0);
  });

  test('delete tweet by tweet id', function () {
    const c1 = mytweetService.createUser(newUser);
    mytweetService.login(newUser);
    newTweet.tweeter = c1._id;
    const t1 = mytweetService.createTweets(c1._id, newTweet);
    assert((mytweetService.getTweetid(t1._id)) != null);
    const t2 = mytweetService.deleteTweetById(t1._id);
    assert((mytweetService.getTweetid(t1._id)) == null);
  });

  test('delete tweet by email', function () {
    const c1 = mytweetService.createUser(newUser);
    mytweetService.login(newUser);
    newTweet.tweeter = c1._id;
    const t1 = mytweetService.createTweets(c1._id, newTweet);
    const t2 = mytweetService.deleteTweetByEmail(t1.name);
    const t3 = mytweetService.getTweetid(t1._id);
    assert.isNull(t3);
  });

  test('get tweet count per user', function () {
    for (let c of users) {
      const c1 = mytweetService.createUser(c);
      mytweetService.login(c);
      newTweet.tweeter = c._id;
      newTweet.name = c.email;
      mytweetService.createTweets(c1._id, newTweet);
    }

    const t = mytweetService.getTweetCount();
    assert.equal(t.length, 3);
    let j = 0;
    for (let i of t) {
      const value = tweets[j].name;
      j++;
      assert.equal(i.count,  1);
      assert.equal(i.email, value);
    }
  });

  test('delete all tweets', function () {
    for (let c of users) {
      const c1 = mytweetService.createUser(c);
      mytweetService.login(c);
      newTweet.tweeter = c._id;
      newTweet.name = c.email;
      mytweetService.createTweets(c1._id, newTweet);
    }

    const t1 = mytweetService.getTweets();
    assert.equal(t1.length, 3);
    const t2 = mytweetService.deleteAllTweets();
    const t3 = mytweetService.getTweets();
    assert.equal(t3.length, 0);
  });

  test('Create tweet by user id', function () {
    const c = mytweetService.createUser(newUser);
    mytweetService.login(newUser);
    const t1 = mytweetService.createTweetByUserId(c._id, newTweet);
    const t2 = mytweetService.getTweets();
    assert.equal(t1._id, t2[0]._id);
  });

  test('delete tweet by Uuid', function () {
    const c = mytweetService.createUser(newUser);
    mytweetService.login(newUser);
    const t1 = mytweetService.createTweets(c._id, newTweet);
    const uuid = 1234567890;
    const t2 = mytweetService.deleteTweetByUuid(uuid);
    const t3 = mytweetService.getTweetid(t1._id);
    assert.isNull(t3);
  });
});
