/**
 * Created by austin on 13/11/2016.
 */
'use strict';

const assert = require('chai').assert;
const MyTweetService = require('./MyTweet-service');
const fixtures = require('./fixtures.json');
const _ = require('lodash');
const bcrypt = require('bcrypt-nodejs');

suite('User API test', function () {

  let users = fixtures.users;
  let newUser = fixtures.newUser;
  let user = {
    email: 'homer@simpson.com',
    password: 'secret',
  };
  const mytweetService = new MyTweetService('http://localhost:4000');

  beforeEach(function () {
    mytweetService.login(user);
    mytweetService.deleteAllUsers();
  });

  afterEach(function () {
    mytweetService.deleteAllUsers();
    mytweetService.logout();
  });

  test('create a user', function () {
    const returnedUser = mytweetService.createUser(newUser);
    assert.equal(returnedUser.firstName, newUser.firstName);
    assert.equal(returnedUser.lastName, newUser.lastName);
    assert.equal(returnedUser.email, newUser.email);
    bcrypt.compare(newUser.password, returnedUser.password, function (err, isValid) {
      assert.equal(isValid, true);
    });

    assert.isDefined(returnedUser._id);
  });

  test('get user', function () {
    const c1 = mytweetService.createUser(newUser);
    mytweetService.login(newUser);
    const c2 = mytweetService.getUser(c1._id);
    assert.deepEqual(c1, c2);
  });

  test('get user by email', function () {
    const c1 = mytweetService.createUser(newUser);
    mytweetService.login(newUser);
    const c2 = mytweetService. getOneUserByEmail(c1.email);
    assert.deepEqual(c1, c2);
  });

  test('get invalid user', function () {
    mytweetService.login(newUser);
    const c1 = mytweetService.getUser('1234');
    assert.isNull(c1);
    const c2 = mytweetService.getUser('012345678901234567890123');//making sure mongoose api doesn't return positive for correct key length
    assert.isNull(c2);
  });

  test('delete a user', function () {
    const c = mytweetService.createUser(newUser);
    mytweetService.login(newUser);
    assert(mytweetService.getUser(c._id) != null);
    mytweetService.deleteOneUser(c._id);
    assert(mytweetService.getUser(c._id) == null);
  });

  test('delete a user by email', function () {
    const c = mytweetService.createUser(newUser);
    mytweetService.login(newUser);
    assert(mytweetService.getOneUserByEmail(c.email) != null);
    mytweetService.deleteOneUserByEmail(c.email);
    assert(mytweetService.getOneUserByEmail(c.email) == null);
  });

  test('get all users', function () {
    for (let c of users) {
      mytweetService.createUser(c);
    }
    mytweetService.createUser(newUser);
    mytweetService.login(newUser);
    const allUsers = mytweetService.getUsers();
    assert.equal(allUsers.length - 1, users.length);
  });

  test('get all users empty', function () {
    mytweetService.createUser(newUser);
    mytweetService.login(newUser);
    const allUsers = mytweetService.getUsers();
    assert.equal(allUsers.length - 1, 0);
  });

  /*test('user Api log on boolean', function () {
    const c1 = mytweetService.createUser(newUser);
    const c2 = {
      email: newUser.email,
      password: newUser.password,
    };
    const b1 = mytweetService.userLoginboolean(c2);
    assert.equal(b1, true);
    const c3 = {
      email: newUser.email,
      password: 'incorrect',
    };
    const b2 = mytweetService.userLoginboolean(c3);
    assert.equal(b2, false);
  });*/

  /*test('user Api log on Android', function () {
    const c1 = mytweetService.createUser(newUser);
    const c2 = {
      email: newUser.email,
      password: newUser.password,
    };
    const b1 = mytweetService.userLoginAndroid(c2);
    assert(_.some([b1], c1), 'returnedUser must be a subset of newUser');
    const c3 = {
      email: newUser.email,
      password: 'incorrect',
    };
    const b2 = mytweetService.userLoginAndroid(c3);
    assert(c1 != b2);
  });*/

  test('user following API', function () {
    const c1 = mytweetService.createUser(newUser);
    for (let c of users) {
      mytweetService.createUser(c);
    }
    mytweetService.login(newUser);
    const allUsers = mytweetService.getUsers();
    const c2 = mytweetService.userFollow(allUsers[1]._id, c1);
    assert.equal(allUsers[1]._id, c2.following[0]._id);

  });

  test('user unfollow API', function () {
    const c1 = mytweetService.createUser(newUser);
    for (let c of users) {
      mytweetService.createUser(c);
    }
    mytweetService.login(newUser);
    const allUsers = mytweetService.getUsers();
    const c2 = mytweetService.userFollow(allUsers[1]._id, c1);
    assert.equal(allUsers[1]._id, c2.following[0]._id);
    const c3 = mytweetService.userUnfollow(c2.following[0]._id, c2);
    assert.equal(c3.following.length, 0);

  });

  test('user settings update API', function () {
    const c1 = mytweetService.createUser(newUser);
    mytweetService.login(newUser);
    c1.firstName = 'Bob';
    const c2 = mytweetService.settings(c1);
    assert.equal(c1.firstName, c2.firstName);
  });
});
