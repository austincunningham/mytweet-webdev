/**
 * Created by austin on 13/11/2016.
 */
'use strict';

const assert = require('chai').assert;
const MyTweetService = require('./MyTweet-service');
const fixtures = require('./fixtures.json');
const _ = require('lodash');
const bcrypt = require('bcrypt-nodejs');
const utils = require('../app/api/utils.js');

suite('Auth API test', function () {

  let users = fixtures.users;
  let tweets = fixtures.tweets;
  let newUser = fixtures.newUser;

  const mytweetService = new MyTweetService('http://localhost:4000');

  beforeEach(function () {
    mytweetService.deleteAllUsers();
  });

  afterEach(function () {
    mytweetService.deleteAllUsers();
  });

  /*  test('login', function () {
      const returnedUser = mytweetService.createUser(newUser);
      const response = mytweetService.login(newUser);
      assert(response.success);
      assert.isDefined(response.token);
    });

    test('verify Token', function () {
      const returnedUser = mytweetService.createUser(newUser);
      const response = mytweetService.login(newUser);

      const userInfo = utils.decodeToken(response.token);
      assert.equal(userInfo.email, returnedUser.email);
      assert.equal(userInfo.userId, returnedUser._id);
    });*/

  test('login-logout', function () {
    mytweetService.createUser(newUser);
    var returnedTweets = mytweetService.getTweets();
    assert.isNull(returnedTweets);

    const response = mytweetService.login(newUser);
    returnedTweets = mytweetService.getTweets();
    assert.isNotNull(returnedTweets);

    mytweetService.logout();
    returnedTweets = mytweetService.getTweets();
    assert.isNull(returnedTweets);
  });
});
