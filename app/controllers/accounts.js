/**
 * Created by austin on 27/09/2016.
 */
'use strict';
const User = require('../models/user');
const Tweet = require('../models/tweet');
const Joi = require('joi');
const bcrypt = require('bcrypt-nodejs');
const disinfect = require('disinfect');
const saltRounds = 10;
const _ = require('lodash');

//default render for /
exports.main = {
  auth: false,
  handler: function (request, reply) {
    reply.view('main', { title: 'Welcome to MyTweet' });
  },

};

//route to administrator home, only admin can login
exports.adminhome = {
  plugins: {
    disinfect: {
      disinfectQuery: true,
      disinfectParams: true,
      disinfectPayload: true,
    },
  },
  handler: function (request, reply) {
    if (request.auth.credentials.loggedInUser == 'admin@mytweet.com') {
      User.find({}).exec().then(allUsers => {
        reply.view('adminhome', {
          title: 'Welcome to Administrator MyTweet',
          user: allUsers,
        });
      }).catch(err => {
        reply.redirect('/');
      });
    }
  },
};

//upload picture to mongodb
exports.userPicUpload = {
  payload: {
    maxBytes: 209715200,
    output: 'stream',
    parse: true,
    allow: 'multipart/form-data',
  },
  handler: function (req, res) {
    var data = req.payload.img;
    var userEmail = req.auth.credentials.loggedInUser;
    User.findOne({ email: userEmail }).then(user => {
      user.img.data = data._data;
      user.img.contentType = 'image/jpeg';
      user.save((err, user) => {
        res.redirect('/settings');
      });
    });
  },
};

// render picture from database
exports.getUserPic = {
  plugins: {
    disinfect: {
      disinfectQuery: true,
      disinfectParams: true,
      disinfectPayload: true,
    },
  },
  handler: function (req, res) {
    //console.log(req.payload);
    var userEmail = req.auth.credentials.loggedInUser;
    User.findOne({ email: userEmail }).then(user => {
      //console.log(user.img.data);
      res(user.img.data).type('image');
    });
  },
};

//render pictures by user id
exports.getUserPicId = {
  plugins: {
    disinfect: {
      disinfectQuery: true,
      disinfectParams: true,
      disinfectPayload: true,
    },
  },
  handler: function (req, res) {
    console.log(req.params.id);
    const id = req.params.id;
    console.log(id);

    //var userEmail = req.auth.credentials.loggedInUser;
    User.findOne({ _id: id }).then(user => {
      //console.log(user.img.data);
      res(user.img.data).type('image');
    });
  },
};

//route to render signup
exports.signup = {
  auth: false,
  handler: function (request, reply) {
    reply.view('signup', {
      title: 'Sign up for MyTweet',

    });
  },

};

//should delete user and tweets
exports.deleteUser = {
  plugins: {
    disinfect: {
      disinfectQuery: true,
      disinfectParams: true,
      disinfectPayload: true,
    },
  },
  handler: function (req, res) {
    const email = req.payload.deleteUser;
    console.log(req.payload);
    User.findOneAndRemove({ email: email }, function (err, tweet) {
      if (err) {
        return res({
          error: 'Error reading tweet: ' + err,
        });
      }

      if (!tweet) {
        return res({ message: '404 not found' });
      }

      //res({ message: `deleted tweet ${req.params.id}` });
      //can't render report every time
      // using the same function for admin delete
      User.find({}).exec().then(allUser => {
        res.view('adminhome', {
          title: 'Welcome to Administrator MyTweet',
          user: allUser,
        });
      });
    });
  },
};

//render login page
exports.login = {
  auth: false,
  handler: function (request, reply) {
    reply.view('login', {
      title: 'Login to MyTweet',
    });
  },
};

// authenticate users , will render user home, authenticate admin will render adminhome
exports.authenticate = {
  plugins: {
    disinfect: {
      disinfectQuery: true,
      disinfectParams: true,
      disinfectPayload: true,
    },
  },
  validate: {

    payload: {
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },

    failAction: function (request, reply, source, error) {
      reply.view('login', {
        title: 'Login error',
        errors: error.data.details,
      }).code(400);
    },

  },
  auth: false,
  handler: function (request, reply) {
    const user = request.payload;
    if (user.email == 'admin@mytweet.com' && user.password == 'secret') {
      request.cookieAuth.set({
        loggedIn: true,
        loggedInUser: user.email,
      });
      reply.redirect('/adminhome');
    } else {

      User.findOne({ email: user.email }).then(foundUser => {
        bcrypt.compare(user.password, foundUser.password, function (err, isValid) {
          if (isValid) {
            request.cookieAuth.set({
              loggedIn: true,
              loggedInUser: user.email,
            });
            reply.redirect('/home');
          } else {
            reply.redirect('/signup');
          }
        });
      }).catch(err => {
        reply.redirect('/');
      });
    };
  },
};

//route to logout, render route /
exports.logout = {
  auth: false,
  handler: function (request, reply) {
    request.cookieAuth.clear();
    reply.redirect('/');
  },

};

//add user to database , validation on form
exports.register = {
  plugins: {
    disinfect: {
      disinfectQuery: true,
      disinfectParams: true,
      disinfectPayload: true,
    },
  },
  validate: {

    payload: {
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      birthday: Joi.date().required(),
      studentNo: Joi.string().max(8),
      carReg: Joi.string().regex(/^\d{2,3}-[A-Z]{1,2}-\d{1,6}$/),
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },

    failAction: function (request, reply, source, error) {
      reply.view('signup', {
        title: 'Sign up error',
        errors: error.data.details,
      }).code(400);
    },

  },
  auth: false,
  handler: function (request, reply) {
        const user = new User(request.payload);
        const myPlaintextPassword = user.password;
        var salt = bcrypt.genSaltSync(saltRounds);
        user.password = bcrypt.hashSync(user.password, salt);
        user.save().then(newUser => {
          reply.redirect('/login');
        }).catch(err => {
          reply.redirect('/');
        });
      },
};

//render current users in settings page
exports.viewSettings = {

  handler: function (request, reply) {
    var userEmail = request.auth.credentials.loggedInUser;
    User.findOne({ email: userEmail }).then(foundUser => {
      reply.view('settings', { title: 'Edit Account Settings', user: foundUser });
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

//update current users settings.
exports.updateSettings = {
  plugins: {
    disinfect: {
      disinfectQuery: true,
      disinfectParams: true,
      disinfectPayload: true,
    },
  },
  validate: {

    payload: {
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },

    failAction: function (request, reply, source, error) {
      reply.view('settings', {
        title: 'Settings error',
        errors: error.data.details,
      }).code(400);
    },

  },
  handler: function (request, reply) {
    const editedUser = request.payload;
    var loggedInUserEmail = request.auth.credentials.loggedInUser;
    User.findOne({ email: loggedInUserEmail }).then(user => {
      user.firstName = editedUser.firstName;
      user.lastName = editedUser.lastName;
      user.email = editedUser.email;
      var salt = bcrypt.genSaltSync(saltRounds);
      user.password = bcrypt.hashSync(editedUser.password, salt);
      return user.save();
    }).then(user => {
      reply.view('settings', { title: 'Edit Account Settings', user: user });
    });
  },
};

exports.oauthLogin = {
  plugins: {
    disinfect: {
      disinfectQuery: true,
      disinfectParams: true,
      disinfectPayload: true,
    },
  },
  auth: 'github-oauth',
  handler: function (request, reply) {

    if (request.auth.isAuthenticated) {

      request.cookieAuth.set(request.auth.credentials);
      return reply.view('github', { title: 'GitHub oAuth likes ' + request.auth.credentials.profile.displayName });
    }

    return reply.view('github', { title: 'Not logged in...' });
  },
};

exports.unfollow = {

  handler: function (req, res) {
    const userEmail = req.auth.credentials.loggedInUser;
    const id = req.params.id;//.slice(0, -1);
    User.findOne({ email: userEmail }).populate('following').then(user => {
      for (let i = 0; i < user.following.length; i++) {
        if (user.following[i].id == id) {
          //var index = user.following.indexOf(id);
          user.following.splice(i, 1);
          console.log(user.following);
          //delete user.following[i];
          return user.save();
        }
      }
    }).then(user => {
      res.redirect('/follow');
    }).catch(err => {
      console.log(err);
      res.redirect('/follow');
    });
  },
};

exports.follow = {
  plugins: {
    disinfect: {
      disinfectQuery: true,
      disinfectParams: true,
      disinfectPayload: true,
    },
  },
  handler: function (req, res) {
      const userEmail = req.auth.credentials.loggedInUser;
      const id = req.params.id;
      let containsid = false;
      console.log(userEmail, id);
      User.findOne({ email: userEmail }).populate('following').then(user => {
        //containsid = _.includes(user.following, id);
        for (let i = 0; i < user.following.length; i++) {
          if (user.following[i].id == id) {
            containsid = true;
            break;
          } else {
            containsid = false;
          }
        };

        if (user.id === id) {
          console.log('do nothing cant follow your self');
        } else if (containsid) {
          console.log('do nothing already following user');
        } else {
          user.following.push(id);
          console.log('before usere save');
          user.save();
          console.log('do i save the user');
        }
      });
      res.redirect('/report');

    },
};

exports.getUserDetails = {
  handler: function (req, res) {
    const id = req.params.id;
    User.findOne({ _id: id }).then(user => {
      return res.user.email;
    });
  },
};

exports.DeleteUserByEmail = {

  handler: function (req, res) {
    console.log('email.params' + req.params.email);
    console.log('do i get here?');
    User.findOneAndRemove({ email: req.params.email }).then(user => {
      if (!user) {
        res(Boom.notFound('id not found'));
      } else {
        console.log(user.firstName);
        res(user);
      }
    }).catch(err => {
      res(Boom.badImplemetation('error accessing Mongo db'));
    });
  },
};

//ajax call to find all users cookie auth
exports.find = {
  handler: function (req, res) {
    User.find({}).exec().then(users => {
      res(users);
    }).catch(err => {
      reply(Boom.badImplemetation('error accessing Mongo db'));
    });
  },
};