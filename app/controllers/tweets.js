'use strict';
const Tweet = require('../models/tweet');
const User = require('../models/user');
const Joi = require('joi');
const Q = require('q');

//route to render view
exports.home = {

  handler: function (request, reply) {
    reply.view('home', { title: 'Make a MyTweet' });
  },

};

// submit a tweet , validation restricts to 140 characters
exports.submit = {
  plugins: {
    disinfect: {
      disinfectQuery: true,
      disinfectParams: true,
      disinfectPayload: true,
    },
  },
  validate: {

    payload: {
      message: Joi.string().max(170),
    },

    options: {
      abortEarly: true,
    },

    failAction: function (request, reply, source, error) {
      reply.view('home', {
        title: 'more that 140 characters',
        errors: error.data.details,
      }).code(400);
    },
  },
  handler: function (request, reply) {
    let data = request.payload;
    console.log(request.payload);
    data.name = request.auth.credentials.loggedInUser;
    User.find({ email: data.name }).then(foundUser => {
      data.tweeter = foundUser[0]._id;
      const tweet = new Tweet(data);
      let date = new Date();
      tweet.date = Number(date);
      tweet.id = Math.floor(Math.random() * 1000000000);
      tweet.save();
      console.log('tweet:' + tweet._id);
      console.log('foundUser:' + foundUser);
      foundUser[0].userTweets.push(tweet._id);
      foundUser[0].save();
    }).then(newTweet => {
      reply.redirect('/mytweetlist');
    }).catch(err => {
      console.log(err);
      reply.redirect('/');
    });
  },
};

//find all tweets and render
exports.report = {
  auth: false,
  handler: function (request, reply) {
    Tweet.find({}).populate('tweeter').then(allTweets => {
      reply.view('report', {
        title: 'MyTweets to Date',
        tweet: allTweets,
      });
    }).catch(err => {
      console.log(err);
      reply.redirect('/');
    });
  },

};

exports.follow = {
  handler: function (request, reply) {
    reply.view('following', { title: 'Following' });
  },
};

exports.followingid = {
  handler: function (req, res) {
    const userEmail = req.auth.credentials.loggedInUser;
    console.log('do i enter following');
    var followingId = [];
    User.find({ email: userEmail }).populate('following').then(foundUser => {
      console.log('do i find user: ' + foundUser);
      if (!foundUser) {
        return res.status(404);
      }

      for (let i = 0; i < foundUser[0].following.length; i++) {
        followingId.push(foundUser[0].following[i].id);
      };

      console.log('do i return ' + followingId);
      return res(followingId);
    }).catch(err => {
      res(Boom.notFound('id not found'));
    });
  },
};

exports.following = {
  auth: false,
  handler: function (req, res) {
    let data = req.payload;
    Tweet.find({ tweeter: Object.keys(data)[0] }).populate('tweeter').exec().then(tweets=> {
      if (!tweets) {
        return res.status(404);
      }

      console.log('individual user tweets should be here? ' + tweets);
      //return tweets;
      //allTweets.push(tweets);
      //console.log('allTweets should appear here but do they return? ' + allTweets);
      return res(tweets);
    });
  },

};

exports.mytweetlist = {

  handler: function (request, reply) {
    var loggedInUserEmail = request.auth.credentials.loggedInUser;
    Tweet.find({ name: loggedInUserEmail }).populate('tweeter').then(allTweets => {
      reply.view('mytweetlist', {
        title: 'MyTweets to Date',
        tweet: allTweets,
      });
    }).catch(err => {
      console.log(err);
      reply.redirect('/');
    });
  },
};

//route to render view finduser
exports.finduser = {

  handler: function (request, reply) {
    reply.view('finduser', { title: 'Search for user Tweets' });
  },

};

//find all tweet by users email address
exports.findusersearch = {
  plugins: {
    disinfect: {
      disinfectQuery: true,
      disinfectParams: true,
      disinfectPayload: true,
    },
  },
  handler: function (request, reply) {
      var findUserEmail = request.payload.name;
      console.log('do i ever get here?' + findUserEmail);
      Tweet.find({ name: findUserEmail }).populate('tweeter').exec().then(allTweets => {
        reply.view('finduser', {
          title: 'MyTweets by Tweeter',
          tweet: allTweets,
        });

      }).catch(err => {
        reply.redirect('/');
      });
    },
};

// delete tweet by id
exports.delete = {
  plugins: {
    disinfect: {
      disinfectQuery: true,
      disinfectParams: true,
      disinfectPayload: true,
    },
  },
  handler: function (req, res) {
    for (let i = 0; i < Object.keys(req.payload).length; i++) {
      let id = Object.keys(req.payload)[i];
      console.log(Object.keys(req.payload).length);
      console.log(id, i);
      Tweet.findOneAndRemove({ _id: id }, function (err, tweet) {
        if (err) {
          return res({
            error: 'Error reading tweet: ' + err,
          });
        }

        if (!tweet) {
          return res({ message: '404 not found' });
        }

        //res({ message: `deleted tweet ${req.params.id}` });
        //can't render report every time
        // using the same function for admin delete
        if (i  == Object.keys(req.payload).length - 1) {
          res.redirect('/mytweetlist');

        }
      });
    }
  },
};

// ajax access to delete tweets by email cookie auth
exports.deleteTweetsByEmail = {
  handler: function (req, res) {
    Tweet.findOneAndRemove({ name: req.params.email }, function (err, tweet) {
      if (err) {
        return res('error accessing Mongo db' + err);
      }

      if (!tweet || tweet === null || tweet === undefined) {
        return res('id not found');
      }

      return res(tweet);
    }).catch(err => {
      res(Boom.badImplemetation('error accessing Mongo db'));
    });
  },
};

// ajax find tweets by users email /api/tweets/email/{email} cookie auth
exports.findUserTweetByEmail = {

  handler: function (req, res) {
    Tweet.find({ name: req.params.email }).then(tweets => {
      res(tweets);
    }).catch(err => {
      res(Boom.badImplemetation('error accessing Mongo db'));
    });
  },
};

// ajax find tweet by id and delete /api/tweets/{id} cookie auth
exports.deleteTweetsById = {
  handler: function (req, res) {
    Tweet.findOneAndRemove({ _id: req.params.id }, function (err, tweet) {
      if (err) {
        return res(Boom.badImplemetation('error accessing Mongo db'));
      }

      if (!tweet) {
        res(Boom.notFound('id not found'));
      }

      res(tweet);
    }).catch(err => {
      res(Boom.badImplemetation('error accessing Mongo db'));
    });
  },
};

//ajax find a tweet by the user id /api/tweets/{id} cookie auth
exports.findUserTweetsById = {
  handler: function (req, res) {
    console.log('do i enter findUserTweetById');
    Tweet.find({ tweeter: req.params.id }).then(tweets => {
      console.log(tweets);
      return res(tweets);
    }).catch(err => {
      res(Boom.badImplemetation('error accessing Mongo db'));
    }).catch(err=> {
      res(Boom.notFound('id not found'));
    });
  },
};
