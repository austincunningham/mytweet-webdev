/**
 * Created by austin on 19/10/2016.
 */
'use strict';

const User = require('../models/user');
const Boom = require('boom');
const bcrypt = require('bcrypt-nodejs');
const disinfect = require('disinfect');
const saltRounds = 10;
const utils = require('./utils.js');

//api to find all users
exports.find = {
  auth: {
    strategy: 'jwt',
  },

  handler: function (req, res) {
    User.find({}).exec().then(users => {
      res(users);
    }).catch(err => {
      reply(Boom.badImplemetation('error accessing Mongo db'));
    });
  },
};

//api to find a single user by id
exports.findOne = {
  auth: {
    strategy: 'jwt',
  },
  handler: function (req, res) {
    User.findOne({ _id: req.params.id }).then(user => {
      if (!user) {
        return res.status(404).end();
      }

      res(user);
    }).catch(err => {
      res(Boom.notFound('id not found'));
    });
  },
};

//api delete a single user by id
exports.delete = {
  auth: {
    strategy: 'jwt',
  },
  handler: function (req, res) {
    User.findOneAndRemove({ _id: req.params.id }, function (err, user) {
      if (err) {
        return (Boom.resbadImplemetation('error accessing Mongo db'));
      }

      if (!user) {
        res(Boom.notFound('id not found'));
      }

      res(user);
    });
  },
};

//api find user by email
exports.findUserByEmail = {
  auth: {
    strategy: 'jwt',
  },
  handler: function (req, res) {
    console.log(req.params.email);
    User.findOne({ email: req.params.email }).then(user => {
      if (!user) {
        res(Boom.notFound('id not found'));
      } else {
        console.log(user.firstName);
        res(user);
      }
    }).catch(err => {
      res(Boom.badImplemetation('error accessing Mongo db'));
    });
  },
};

//api find user by email and delete
exports.DeleteUserByEmail = {
  auth: {
    strategy: 'jwt',
  },

  handler: function (req, res) {
    console.log('email.params' + req.params.email);
    console.log('do i get here?');
    User.findOneAndRemove({ email: req.params.email }).then(user => {
      if (!user) {
        res(Boom.notFound('id not found'));
      } else {
        console.log(user.firstName);
        res(user);
      }
    }).catch(err => {
      res(Boom.badImplemetation('error accessing Mongo db'));
    });
  },
};

//delete all users
exports.deleteAll = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    User.remove({}).then(err => {
      reply().code(204);
    }).catch(err => {
      reply(Boom.badImplementation('error removing Users'));
    });
  },

};

//api add new user
exports.register = {
  auth: false,
  handler: function (request, reply) {
    const user = new User(request.payload);
    const myPlaintextPassword = user.password;
    var salt = bcrypt.genSaltSync(saltRounds);
    user.password = bcrypt.hashSync(user.password, salt);
    user.save().then(newUser => {
      //reply.redirect('/adminhome');
      reply(newUser);
    }).catch(err => {
      //console.log(request.auth.credentials.loggedInUser);
      reply(Boom.badImplementation('error creating user '));
    });
  },
};

exports.login = {
  auth: false,
  handler: function (request, reply) {
    const user = request.payload;
    User.findOne({ email: user.email }).then(foundUser => {
      bcrypt.compare(user.password, foundUser.password, function (err, isValid) {
        if (isValid) {
          const token = utils.createToken(foundUser);
          reply({ success: true, token: token, user: foundUser }).code(201);
        }else {
          reply({ success: false, message: 'Authentication failed. User not found.' }).code(201);
        }
      });
    }).catch(err => {
      reply(Boom.notFound('internal db failure'));
    });
  },
};

exports.androidLogin = {
  auth: false,
  handler: function (request, reply) {
    const user = request.payload;
    User.findOne({ email: user.email }).then(foundUser => {
      bcrypt.compare(user.password, foundUser.password, function (err, isValid) {
        if (isValid) {
          return reply(foundUser);
        }else {
          reply(Boom.notFound('id not found'));
        }
      });
    }).catch(err => {
      reply(Boom.badImplementation('id not found'));
    });
  },
};

exports.follow = {
  auth: {
    strategy: 'jwt',
  },
  handler: function (req, res) {
    const userEmail = req.payload.email;
    const id = req.params.id;
    let containsid = false;
    console.log(userEmail, id);
    User.findOne({ email: userEmail }).populate('following').then(user => {
      //containsid = _.includes(user.following, id);
      for (let i = 0; i < user.following.length; i++) {
        if (user.following[i].id == id) {
          containsid = true;
          break;
        } else {
          containsid = false;
        }
      };

      if (user.id === id) {
        console.log('do nothing cant follow your self');
      } else if (containsid) {
        console.log('do nothing already following user');
      } else {
        user.following.push(id);
        console.log('before user save');
        user.save();
        console.log('do i save the user');
      }

      res(user);
    }).catch(err => {
      res(err);
    });

  },
};

exports.unfollow = {
  auth: {
    strategy: 'jwt',
  },
  handler: function (req, res) {
    const userEmail = req.payload.email;
    const id = req.params.id;
    console.log(userEmail, id);
    User.findOne({ email: userEmail }).populate('following').then(user => {
      //containsid = _.includes(user.following, id);
      for (let i = 0; i < user.following.length; i++) {
        if (user.following[i].id == id) {
          user.following.splice(i, 1);
          user.save();
          console.log('do i save the user');
        }
      };

      res(user);
    }).catch(err => {
      res(err);
    });

  },
};

exports.settings = {
  auth: {
    strategy: 'jwt',
  },
  handler: function (request, reply) {
    const editedUser = request.payload;
    User.findOne({ _id: request.payload._id }).then(user => {
      user.firstName = editedUser.firstName;
      user.lastName = editedUser.lastName;
      user.email = editedUser.email;
      var salt = bcrypt.genSaltSync(saltRounds);
      user.password = bcrypt.hashSync(editedUser.password, salt);
      return user.save();
    }).then(user => {
      reply(user);
    });
  },
};
