/**
 * Created by austin on 19/10/2016.
 */
'use strict';

const Tweet = require('../models/tweet');
const User = require('../models/user');
const Boom = require('boom');

//api find all tweets /api/tweets
exports.find = {
  auth: false,

  handler: function (req, res) {
    Tweet.find({}).exec().then(tweets => {
      res(tweets);
    }).catch(err => {
      reply(Boom.badImplemetation('error accessing Mongo db'));
    });
  },
};

//api find a tweet by id /api/tweet/{id}
exports.findTweetById = {
  auth: {
    strategy: 'jwt',
  },
  handler: function (req, res) {
    console.log('do i enter findTweetById');
    Tweet.findOne({ _id: req.params.id }).exec().then(tweet => {
        if (!tweet) {
          return res.status(404);
        }

        res(tweet);
      }).catch(err => {
      res(Boom.notFound('id not found'));
    });
  },
};

//api find a tweet by the user id /api/tweets/{id}
exports.findUserTweetById = {
  auth: {
    strategy: 'jwt',
  },
  handler: function (req, res) {
    console.log('do i enter findUserTweetById');
    Tweet.find({ tweeter: req.params.id }).then(tweets => {
      console.log(tweets);
      return res(tweets);
    }).catch(err => {
      res(Boom.badImplemetation('error accessing Mongo db'));
    }).catch(err=> {
      res(Boom.notFound('id not found'));
    });
  },
};

//counting number of tweets function
function counting(tweets, email, count) {
  let i = 0;
  while (i < tweets.length) {
    //for (var i = 0; i < tweets.length; i++) {
    if (tweets[i].name === email) {
      count++;

    }

    i++;
  }

  return count;
};

//API to return an array[{email:value, count:value},{...etc}] /api/tweetcount/
exports.findUserTweetCount = {
  auth: false,
  handler: function (req, res) {
      const tweetCount = [];
      let count = 0;
      let exists;
      Tweet.find({}).exec().then(tweets => {
          console.log('\ntweets : ' + tweets);
          for (let i = 0; i < tweets.length; i++) {
            let email = tweets[i].name;
            console.log('\nemail :' + email);
            count = counting(tweets, email, count);
            console.log('\ncount :' + count);

            //check to see if email exists in new object tweetCount, if it does don't add obj
            for (var j = 0; j < tweetCount.length; j++) {
              if (tweetCount[j].email === email) {
                exists = true;
                count = 0;
                break;
              }else {
                exists = false;
              }
            }

            if (exists) {
              console.log('dont push');
            } else {
              tweetCount.push({ email, count });
              count = 0;
              exists = false;
            }

            console.log('\ntweetCount array : ' + tweetCount);
          }

          return res(tweetCount);
        });

    },
};

// api find tweets by users email /api/tweets/email/{email}
exports.findUserTweetByEmail = {
  auth: {
    strategy: 'jwt',
  },

  handler: function (req, res) {
    Tweet.find({ name: req.params.email }).then(tweets => {
      res(tweets);
    }).catch(err => {
      res(Boom.badImplemetation('error accessing Mongo db'));
    });
  },
};

// api find tweet by id and delete /api/tweets/{id}
exports.delete = {
  auth: {
    strategy: 'jwt',
  },
  handler: function (req, res) {
    Tweet.findOneAndRemove({ _id: req.params.id }, function (err, tweet) {
      if (err) {
        return res(Boom.badImplemetation('error accessing Mongo db'));
      }

      if (!tweet) {
        res(Boom.notFound('id not found'));
      }

      res(tweet);
    }).catch(err => {
      res(Boom.badImplemetation('error accessing Mongo db'));
    });
  },
};

// api find tweets by email and delete /api/tweets/email/{email}
exports.deleteTweetsByEmail = {
  auth: {
    strategy: 'jwt',
  },
  handler: function (req, res) {
    Tweet.findOneAndRemove({ name: req.params.email}, function (err, tweet) {
      if (err) {
        return res('error accessing Mongo db' + err);
      }

      if (!tweet || tweet === null || tweet === undefined) {
        return res('id not found');
      }

      return res('success');
    }).catch(err => {
      res(Boom.badImplemetation('error accessing Mongo db'));
    });
  },
};

//delete all tweets /api/tweets
exports.deleteAll = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    Tweet.remove({}).then(err => {
      reply().code(204);
    }).catch(err => {
      reply(Boom.badImplementation('error removing Tweets'));
    });
  },

};

//create tweet by user id /api/tweet/{id}
exports.newTweetById = {
  auth: {
    strategy: 'jwt',
  },
  handler: function (request, reply) {
    const tweet = new Tweet(request.payload);
    tweet.tweeter = request.params.id;
    tweet.save().then(newTweet => {
      reply(newTweet);
    }).catch(err => {
      //console.log(request.auth.credentials.loggedInUser);
      reply(Boom.badImplementation('error creating Tweet '));
    });
  },
};

// api find tweet by uuid and delete /api/tweets/{uuid}
exports.deleteUuid = {
  auth: {
    strategy: 'jwt',
  },
  handler: function (req, res) {
    Tweet.findOneAndRemove({ id: req.params.uuid }, function (err, tweet) {
      if (!tweet) {
        return res(Boom.notFound('id not found'));
      } else {
        return res(tweet);
      }
    }).catch(err => {
      res(Boom.notFound('error accessing Mongo db'));
    });
  },
};

//find users following id
exports.followingid = {
  auth: {
    strategy: 'jwt',
  },
  handler: function (req, res) {
    const userEmail = req.param;
    console.log('do i enter following');
    var followingId = [];
    User.find({ email: userEmail }).populate('following').then(foundUser => {
      console.log('do i find user: ' + foundUser);
      if (!foundUser) {
        return res.status(404);
      }

      for (let i = 0; i < foundUser[0].following.length; i++) {
        followingId.push(foundUser[0].following[i].id);
      };

      console.log('do i return ' + followingId);
      return res(followingId);
    }).catch(err => {
      res(Boom.notFound('id not found'));
    });
  },
};

// find the tweets from the following ids
exports.following = {
  auth: {
    strategy: 'jwt',
  },
  handler: function (req, res) {
    let data = req.payload;
    Tweet.find({ tweeter: Object.keys(data)[0] }).populate('tweeter').exec().then(tweets=> {
      if (!tweets) {
        return res.status(404);
      }

      console.log('individual user tweets should be here? ' + tweets);

      //return tweets;
      //allTweets.push(tweets);
      //console.log('allTweets should appear here but do they return? ' + allTweets);
      return res(tweets);
    });
  },

};

